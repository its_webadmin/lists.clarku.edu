# lists.clarku.edu
================

This is a complete CSS replacement for lists.clarku.edu.

## Dependencies

This project relies on [node.js](http://nodejs.org/). Once it's installed you'll need to install the Grunt CLI globally as well with `npm`:

    npm install -g grunt-cli

After that, you'll need to get the node modules for the project:

    npm install

## Deploying

To deploy, ensure the `scp_dest` in the `package.json` file is correct, this is where the `style.css` file will be deployed to. After that, just run:

    grunt

Since it's using SCP to deplay `style.css`, you'll be prompted to authenticate with the server the file is being copied to.

## Watching for Changes

Running `grunt watch` will watch all files in `src/less` for changes, and upon a change re-compile the CSS and SCP it to where it needs to be.

*[CLI]: Command Line Interface
*[CSS]: Cascading Style Sheets
*[SCP]: Secure Copy