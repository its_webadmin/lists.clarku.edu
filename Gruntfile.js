(function () {
  'use strict';

  module.exports = function (grunt) {
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
      banner: '/*!\n' +
              ' * <%= pkg.name %> v<%= pkg.version %> (<%= pkg.homepage %>)\n' +
              ' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>\n' +
              ' * Licensed under <%= pkg.license %> (<%= pkg.homepage %>/blob/master/LICENSE)\n' +
              ' */\n',

      less: {
        screen: {
          files: {
            'dist/style/style.css': 'src/less/style.less'
          },
          options: {
            banner: '<%= banner %>',
            compress: true,
            strictUnits: true
          }
        }
      },
      shell: {
        options: { stderr: false },

        scp: { command: 'scp -r dist/* <%= pkg.scp_dest %>' }
      },
      watch: {
        less: {
          files: ['src/less/**/*.less'],
          tasks: ['less', 'shell:scp']
        }
      }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-shell');

    grunt.registerTask('default', ['less', 'shell:scp']);
  };
}());